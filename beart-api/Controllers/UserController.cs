﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using beart_api.Model;
using beart_api.Context;
using Microsoft.Extensions.DependencyInjection;

namespace beart_api.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("[controller]")]
    public class UserController : Controller
    {
        private readonly IDbContext _context;

        public UserController(IDbContext dbContext)
        {
            _context = dbContext;
        }

        // GET api/user
        [HttpGet]
        public Task<IEnumerable<User>> Get()
        {
            return ReadAllUsers();
        }

        // POST api/user
        [HttpPost]
        public User Post([FromBody] string value)
        {
            var NewUser = new User();
            //return CreateUser();
            Console.WriteLine(value);
            return NewUser;
        }

        private async Task<IEnumerable<User>> ReadAllUsers()
        {
            return await _context.ReadAllUsers() ?? null;
        }
    }
}
