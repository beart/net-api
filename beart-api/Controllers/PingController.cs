﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using beart_api.Model;
using beart_api.Context;
using Microsoft.Extensions.DependencyInjection;

namespace beart_api.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("[controller]")]
    public class PingController : Controller
    {
        private readonly IDbContext _context;

        public PingController(IDbContext dbContext)
        {
            _context = dbContext;
        }

        [HttpGet]
        public Ping Get()
        {
            var answer = new Ping();
            answer.Answer = "Pong";
            return answer;
        }
    }
}
