﻿using beart_api.DBSettings;
using beart_api.Model;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Bson;

namespace beart_api.Context
{
    public interface IDbContext
    {
        Task<IEnumerable<User>> ReadAllUsers();
    }

    public class DbContext : IDbContext
    {
        private readonly IMongoDatabase _database = null;
        public DbContext(IOptions<Settings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Value.Database);
        }


        public async Task<IEnumerable<User>> ReadAllUsers()
        {
            return await _database.GetCollection<User>("users").Find(_ => true).ToListAsync();
        }
    }
}
